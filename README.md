# Symfony coding standards

## Installation

- Add bitbucket repository into your composer.json

```
"repositories":[
    {
      "type": "git-bitbucket",
      "url" : "https://bitbucket.org/centurygenomics/symfony-coding-standard.git"
    }
 ]
```

- Install package

```
composer require --dev centurygenomics/symfony-coding-standard
```

## Configure PHP Code Sniffer in PhpStorm

- Copy "phpcs.xml.dist" to "phpcs.xml" in your project root.
- Go to Preferences>Editor>Inspections>PHP_CodeSniffer validation.
- Select "Custom" under "Coding standard".
- Select phpcs.xml in your project root.
